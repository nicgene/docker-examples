<?php

try {
    $pdo = new PDO('mysql:host=mariadb;dbname=information_schema;port=3306;charset=utf8mb4', 'root', 'password');
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

echo '<b>Hostname:</b> ' . getenv('HOSTNAME') . '<br>';

echo '<b>MariaDB Version:</b> ' . $pdo->query('select version()')->fetchColumn() . '<br>';
