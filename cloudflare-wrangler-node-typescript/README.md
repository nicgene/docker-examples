# Docker, Node and TypeScript for Cloudflare Workers
This project demonstrates how to scaffold a serverless execution environment on the Cloudflare edge network using Node and TypeScript.

## Caveats
- Installing `@cloudflare/wrangler` globally from NPM fails at the postinstall script. Instead, wrangler is installed from binary in the Dockerfile.
- `wrangler dev` currently throws Error: EOF on most Mac and Linux systems. [#1583](https://github.com/cloudflare/wrangler/issues/1583)

## Setup
Create your own wrangler.toml file from the example. Add your `account_id` and `zone_id`.

## Install the dependencies
```bash
yarn install
```

## Run service offline
```bash
docker-compose up
```

## Re-build the image
```bash
docker-compose up --build
```

## Notes
You can access the shell using the following command:

```bash
docker exec -it cloudflare-wrangler-node-typescript_app_1 /bin/ash
```

## Deployment
If you haven't alread, create a Cloudflare Workers API Token by going to your [profile page](https://dash.cloudflare.com/profile/api-tokens) and following the instructions on [generating tokens](https://developers.cloudflare.com/workers/cli-wrangler/configuration).

After you access the container's shell you can configure wrangler with a Cloudflare API Token by running `wrangler config`.

**Alternatively**, use [environment variables](https://developers.cloudflare.com/workers/cli-wrangler/configuration#environments) to configure `CF_API_TOKEN` in your `wrangler.toml` file for specific environments.

Deploy your workers to Cloudflare.
```bash
wrangler publish
```
